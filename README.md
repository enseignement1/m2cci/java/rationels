# Rationals 

Ce projet est une correction du [TP nombres rationnels](https://lig-membres.imag.fr/genoud/teaching/PL2AI/tds/POO/sujets/tp04_NombresRationnels/tp04_NombresRationnels.html de l'enseignement POO du M2CCI.

Ce projet a été crée sous VSCode avec l'assistant de  l'[Extension Pack for Java](https://marketplace.visualstudio.com/items?itemName=vscjava.vscode-java-pack). Sa structure est la suivante :  

## Getting Started

Welcome to the VS Code Java world. Here is a guideline to help you get started to write Java code in Visual Studio Code.

## Folder Structure

The workspace contains two folders by default, where:

- `src`: the folder to maintain sources
- `lib`: the folder to maintain dependencies

Meanwhile, the compiled output files will be generated in the `bin` folder by default.

> If you want to customize the folder structure, open `.vscode/settings.json` and update the related settings there.

## Dependency Management

The `JAVA PROJECTS` view allows you to manage your dependencies. More details can be found [here](https://github.com/microsoft/vscode-java-dependency#manage-dependencies).
