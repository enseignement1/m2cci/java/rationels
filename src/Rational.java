/**
 * <p>
 * La classe Rational représente les nombres rationnels.
 * </p>
 * <p>
 * Un nombre rationnel défini par un objet Rational est toujours représenté sous sa forme
 * irréductible (simplifiée) et avec un dénominateur positif.
 * </p>
 * <p>
 * Seuls des nombres rationnels valides peuvent être représentés par cette classe. Un objet Rational
 * ne peut avoir un denominateur nul.
 * </p>
 *
 * @author Equipe pédagogique Ensimag - POO
 * @version 1.0 modifications by Philippe Genoud - UGA - LIG STeamer
 */
public class Rational {

    /**
     * numérateur
     */
    private int num;
    /**
     * dénominateur
     */
    private int denom;

    /**
     * Construit un nouveau rationnel num/denom et le stocke sous sa forme simplifiée.
     *
     * @param num numerator
     * @param denom denominator
     * @throws IllegalArgumentException si denom est nul.
     */
    public Rational(int num, int denom) {
        if (denom == 0) {
            throw new IllegalArgumentException("dénominateur nul interdit");
        }
        this.num = num;
        this.denom = denom;
        this.simplifier();
    }

    /**
     * Construit le rationnel n/1.
     *
     * @param n numerator
     */
    public Rational(int n) {
        // on pourrait appeler le constructeur précédent par l'instruction
        // this(n, 1);
        // mais cela ferait faire des traitements qui dans ce cas sont inutiles
        this.num = n;
        this.denom = 1;
    }

    /**
     * Constructor avec copie, le nouveau Rationnel créé est intialisé avec une valeur de numérateur
     * et dénominateur identiques à celle du rationnel argument du constructeur.
     *
     * @param other le rationnel utilisé pour initialiser le nouveau rationnel créé.
     */
    public Rational(Rational other) {
        // on pourrait appeler le premier constructeur par l'instruction
        // this(other.num, other.denom);
        // comme other est déjà un rationnel (son dénomiteur n'est pas nul,
        // il est déjà en forme simplifiée) c'est inutile de refaire les contrôles
        // et les calculs
        this.num = other.num;
        this.denom = other.denom;
    }

    /**
     * Retourne le numérateur de ce rationnel.
     *
     * @return le numerateur du rationnel.
     */
    public int getNum() {
        return this.num;
    }

    /**
     * Retourne le dénominateur de ce rationnel.
     *
     * @return le dénominateur du rationnel.
     */
    public int getDenom() {
        return this.denom;
    }

    /**
     * Convertit la valeur de ce rationnel en un double.
     *
     * @return la valeur double précision la plus proche de la valeur réprésentée par ce nombre
     *         rationel.
     */
    public double toDouble() {
        return (double) num / denom;
    }

    /**
     * Multiplie ce rationnel (this) avec un autre rationnel. Le rationnel multiplié (this) est
     * modifié , après exécution de cette méthode son numérateur et son dénominateur correspondent
     * au numérateur et dénominateur du produit.
     *
     * @param r le rationnel avec lequel ce rationel est multiplié.
     * @return this, permet d'enchaîner les opérations. grace à cela, on peut châiner les opérations
     *         et écrire r1.add(r2).mult(r3); plutôt que r1.add(r2); r1.mult(r3);
     */
    public Rational mult(Rational r) {
        this.num = this.getNum() * r.getNum();
        this.denom = this.getDenom() * r.getDenom();
        this.simplifier();
        return this;
    }

    /**
     * Ajoute un rationnel à ce rationnel (this). Le rationnel additionné (this) est modifié , après
     * exécution de cette méthode son numérateur et son dénominateur correspondent au numérateur et
     * dénominateur de la somme.
     * 
     * @param r le rationnel à ajouter.
     * @return this, permet d'enchaîner les opérations. grace à cela, on peut châiner les opérations
     *         et écrire r1.add(r2).mult(r3); plutôt que r1.add(r2); r1.mult(r3);
     */
    public Rational add(Rational r) {
        this.num = r.getDenom() * this.getNum() + this.getDenom() * r.getNum();
        this.denom = r.getDenom() * this.getDenom();
        this.simplifier();
        return this;
    }

    /**
     * simplifie ce rationel pour le mettre sous une forme irreductible avec un denominateur
     * positif.
     */
    private void simplifier() {
        int pgcd = pgcd(this.num, this.denom);
        this.num /= pgcd;
        this.denom /= pgcd;
        if (this.denom < 0) {
            this.num = -this.num;
            this.denom = -this.denom;
        }
    }

    /**
     * renvoie le plus grand commun diviseur (pgcd) entre deux entiers
     *
     * @param a le premier entier
     * @param b le second entier
     * @return le pgcd de a et b
     */
    private int pgcd(int a, int b) {
        return (b == 0) ? a : pgcd(b, a % b);
        // expression conditionnelle, equivalent à
        // if (b == 0) {
        // return a;
        // } else {
        // return pgcd(b, a % b);
        // }
    }

    /**
     * donne une représentation du rationnel sous forme d'une chaîne de caractères
     *
     * @return la chaîne représentant le rationel num/denom
     */
    @Override
    public String toString() {
        return this.getNum() + (this.getDenom() != 1 ? "/" + this.getDenom() : "");
        // l'expression conditionnelle ci-dessus évite d'afficher le dénominateur si il vaut 1
    }

    /**
     * teste l'égalité de ce rationnel (this) avec un autre rationnel
     *
     * (on verra plus tard que ce n'est pas tout à fait comme cela que la méthode d'égalité devrait
     * être définie, mais il va falloir d'abord aborder les notions d'héritage et de polymorphisme).
     *
     * @param other l'autre rationnel.
     *
     * @return true si ce rationnel a le même numérateur et le même dénonimateur que other, false
     *         sinon.
     */
    public boolean egale(Rational other) {
        return (other != null && this.getNum() == other.getNum()
                && this.getDenom() == other.getDenom());
    }

}
