public class TestRational {
    public static void main(String[] args) throws Exception {
        Rational r1 = new Rational(6, 4);
        System.out.println("r1 = " + r1.toString());
        System.out.println("sous la forme réelle, r1 = " + r1.toDouble());

        Rational r2 = new Rational(2);
        System.out.println("r2 = " + r2);

        System.out.println("r1 = r1 + r2 = " + r1.add(r2));

        Rational r3 = new Rational(34, 8);
        System.out.println("r3 = " + r3);

        System.out.print(r1 + " x " + r3);
        r1.mult(r3);
        System.out.println(" = " + r1);

        System.out.println(r2 + " x " + r3 + " = " + r2.mult(r3));

        Rational r4 = new Rational(2, 4);
        Rational r5 = new Rational(-6, -12);
        System.out.println("r4 = " + r4 + "\nr5 = " + r5);
        System.out.println("le test d'égalité de r4 et r5 donne " + r4.egale(r5));

        // Rational r6 = new Rational(10,0);
        // La ligne ci dessus si elle est décommentée provoque une erreur (Exception)
        // qui arrête l'exécution
        // conformément à ce qui était prévu.
        // Exception in thread "main" java.lang.IllegalArgumentException: dénominateur
        // nul interdit
        // at fr.im2ag.cci.rationals.Rational.<init>(Rational.java:40)
        // at fr.im2ag.cci.rationals.RationalTest.main(RationalTest.java:37)
        // ...
    }
}
