import java.util.Scanner;

public class TestSequenceRationals {

    private static Scanner sc = new Scanner(System.in);

    public static int lireEntier(String message) {
        System.out.print(message);
        return Integer.parseInt(sc.nextLine());
    }

    /**
     * permet de lire un nombre rationnel en le rentrant comme une fraction
     * @param message le message d'invite à afficher
     * @return le nombre rationnel lu
     */
    public static Rational lireRational(String message) {
        System.out.print(message);
        String[] tokens = sc.nextLine().split("/");
        return new Rational(Integer.parseInt(tokens[0].trim()), Integer.parseInt(tokens[1].trim()));
    }

    public static boolean lireOuiNon(String message) {
        System.out.print(message);
        return sc.nextLine().toUpperCase().charAt(0) == 'O';
    }

    public static void main(String[] args) throws Exception {

        System.out.println("test de SequenceInt");
        SequenceRationals seq1 = new SequenceRationals();
        SequenceRationals seq2 = new SequenceRationals(seq1);
        System.out.println("sequence 1 : " + seq1); // équivalent à System.out.print("sequence 1 : " + seq1.toString());
        System.out.println("sequence 2 : " + seq2);

        System.out.println();
        System.out.println("ajout des valeurs 1/2 , 2/3 et 3/4 en tête de sequence 1");
        seq1.ajouterEnTête(new Rational(1,2)).ajouterEnTête(new Rational(2, 3)).ajouterEnTête(new Rational(3, 4));
        System.out.println("sequence 1 : " + seq1);

        System.out.println();
        System.out.println("ajout des valeurs 4/5 , 5/6 et 6/7 en queue de sequence 1");
        seq1.ajouterEnQueue(new Rational(4,5)).ajouterEnQueue(new Rational(5,6)).ajouterEnQueue(new Rational(6,7));
        System.out.println("sequence 1 : " + seq1 + " longueur : " + seq1.getLongueur());
        System.out.println("sequence 2 n'a pas été modifiée");
        System.out.println("sequence 2 : " + seq2 + " longueur : " + seq2.getLongueur());

        seq2 = new SequenceRationals(seq1);
        System.out.println("sequence 2 : " + seq2 + " longueur : " + seq2.getLongueur());

        System.out.println();
        System.out.println("test getValeurDeRang sur sequence 1");
        int rang = lireEntier("donner un rang entre 1 et " + seq1.getLongueur() + " (-1 pour terminer) : ");
        while (rang != -1) {
            System.out.println("la valeur de l'élément est " + seq1.getValeurDeRang(rang));
            rang = lireEntier("donner un rang entre 1 et " + seq1.getLongueur() + " (-1 pour terminer) : ");
        }

        System.out.println();
        System.out.println("test de retirerPremierElt sur sequence 1");
        while (lireOuiNon("retirer le premier élément de sequence 1 ? O/n : ")) {
            System.out.println("valeur élément retiré : " + seq1.retirerPremierElt());
            System.out.println("sequence 1 : " + seq1 + " longueur : " + seq1.getLongueur());
        }

        System.out.println();
        System.out.println("test de retirerDernierElt sur sequence 1");
        while (lireOuiNon("retirer le dernier élément de sequence 1 ? O/n : ")) {
            System.out.println("valeur élément retiré : " + seq1.retirerDernierElt());
            System.out.println("sequence 1 : " + seq1 + " longueur : " + seq1.getLongueur());
        }

        System.out.println();
        System.out.println("test rangDe sur sequence 1");
        do {
            Rational val = lireRational("valeur recherchée : ");
            System.out.println("rang de première occurence de cette valeur " + seq1.rangDe(val));
        } while (lireOuiNon("autre recherche ? O/n"));

        System.out.println();
        System.out.println("test rangDe à partir d'un rang donné sur sequence 1");
        rang = lireEntier("donner un rang entre 1 et " + seq1.getLongueur() + " (-1 pour terminer) : ");
        while (rang != -1) {
            Rational val = lireRational("valeur recherchée : ");
            System.out.printf("rang de première occurence de cette valeur à partir de %d : %d\n", rang,
                    seq1.rangDe(val, rang));
            rang = lireEntier("donner un rang entre 1 et " + seq1.getLongueur() + " (-1 pour terminer) :");
        }

        System.out.println();
        System.out.println("création de sequence 3 qui contient les valeurs de rang impair de sequence 2");
        System.out.println("sequence 2 : " + seq2);
        SequenceRationals seq3 = seq2.sequenceRangsImpair();
        System.out.print("sequence 3 : " + seq3);
    }
}

