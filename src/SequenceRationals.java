/**
 * Sequence de Rationals réprésentée à l'aide d'un liste (doublement) chaînée
 */
public class SequenceRationals {

    // --------------------------------------------------------------------------------------------
    // classe imbriquée (nested class) pour définir les éléments d'une séquence de Rationals.
    // On aurait pu déclarer cette classe dans un fichier .java à part, mais ici en la déclarant
    // comme étant privée, elle est à seul usage interne de la classe SequenceRationals.
    // --------------------------------------------------------------------------------------------
    /**
     * Elément d'une séquence de rationnels. Un élement est un couple valeur (un Rational) et deux
     * "pointeurs" (références)
     * 
     * - suivant : pointeur vers l'élément suivant dans la séquence
     * 
     * - précédent : pointeur vers l'élément précédent dans la séquence
     */
    private static class ElementSeqRationals {

        private Rational valeur; // la valeur de l'élément
        private ElementSeqRationals suivant; // référence de l'élément suivant, null si pas de
                                             // suivant
        private ElementSeqRationals précédent; // référence de l'élément précédent, null si pas de
                                               // précédent

        public ElementSeqRationals(ElementSeqRationals précédent, Rational valeur,
                ElementSeqRationals suivant) {
            this.valeur = valeur;
            this.suivant = suivant;
            this.précédent = précédent;
        }

        public Rational getValeur() {
            return valeur;
        }

        public ElementSeqRationals getSuivant() {
            return suivant;
        }

        public void setSuivant(ElementSeqRationals suivant) {
            this.suivant = suivant;
        }

        public ElementSeqRationals getPrécédent() {
            return précédent;
        }

        public void setPrécédent(ElementSeqRationals précédent) {
            this.précédent = précédent;
        }
    }
    // -----------------------------------------------------------------------------------
    // Attributs (propriétés, varaibles d'instance) d'un objet SequenceRationals
    // -----------------------------------------------------------------------------------

    /**
     * Référence sur l'objet tête de la liste (doublement) chaînée représentant la séquence
     */
    private ElementSeqRationals tête = null;

    /**
     * Référence sur l'objet en queue de la liste (doublement) chaînée représentant la séquence
     * (afin de faciliter les insertions en fin et éviter de faire un parcours complet de la liste à
     * chaque fois).
     */
    private ElementSeqRationals queue = null;

    /**
     * Nombre d'éléments de la séquence. On aurait pu se passer de cette variable et se contenter
     * d'une fonction de calcul de la longueur parcourant la liste chaînée. Mais il a été jugé plus
     * efficace, toujours pour éviter des parcours de liste, de maintenir cette valeur dans un
     * attribut.
     */
    private int longueur = 0;

    // -----------------------------------------------------------------------------------
    // Constructeurs de la classe SequenceRationalss
    // -----------------------------------------------------------------------------------

    /**
     * Création d'une séquence vide
     */
    public SequenceRationals() {
        // Ce constructeur est vide, il correspond au constructeur par défaut, il
        // est nécessaire de le déclarer explicitement car il existe un deuxième
        // constucteur
    }

    /**
     * Création d'une séquence en faisant une copie d'une autre séquence
     * 
     * @param seq la séquence à copier
     */
    public SequenceRationals(SequenceRationals seq) {

        ElementSeqRationals eltCourantdeSeq = seq.tête;
        while (eltCourantdeSeq != null) {
            this.ajouterEnQueue(eltCourantdeSeq.getValeur());
            eltCourantdeSeq = eltCourantdeSeq.getSuivant();
        }

        // Une autre manière d'écrire ce code avec une boucle do while
        // if (!seq.estVide()) {
        // ElementSeqRationals eltCourantdeSeq = seq.tête;
        // do {
        // this.ajouterEnTête(eltCourantdeSeq.getValeur());
        // eltCourantdeSeq = eltCourantdeSeq.getSuivant();
        // } while (eltCourantdeSeq != null);

    }

    // -------------------------------------------------------------------------------------
    // Méthodes définissant les messages pouvant être envoyés à des objets
    // SequenceRationalss
    // --------------------------------------------------------------------------------------

    /**
     * ajoute un élement en tête de séquence
     * 
     * @param valeur la valeur de l'élément à ajouter
     * @return la séquence (permet de chaîner les appels : ainsi pour ajouter deux valeurs à une
     *         sequence on pourra écrire seq.ajouterEnTête(val1).ajouterEntête(val2) plutôt que
     *         seq.ajouterEnTête(val1); puis seq.ajouterEntête(val2);
     */
    public SequenceRationals ajouterEnTête(Rational valeur) {
        if (this.estVide()) {
            tête = new ElementSeqRationals(null, valeur, null);
            queue = tête;
        } else {
            ElementSeqRationals elt = new ElementSeqRationals(null, valeur, tête);
            tête.setPrécédent(elt);
            tête = elt;
        }
        longueur++;
        return this;
    }

    /**
     * ajoute un élement en fin de séquence
     * 
     * @param valeur la valeur de l'élément à ajouter
     * @return la séquence (permet de chaîner les appels : ainsi pour ajouter deux valeurs à une
     *         sequence on pourra écrire seq.ajouterEnQueue(val1).ajouterEnQueue(val2) plutôt que
     *         seq.ajouterEnQueue(val1); puis seq.ajouterEnQueue(val2);
     */
    public SequenceRationals ajouterEnQueue(Rational valeur) {
        if (this.estVide()) {
            tête = new ElementSeqRationals(null, valeur, null);
            queue = tête;
        } else {
            ElementSeqRationals elt = new ElementSeqRationals(queue, valeur, null);
            queue.setSuivant(elt);
            queue = elt;
        }
        longueur++;
        return this;
    }

    /**
     * @return la longueur (nombre d'éléments de la séquence)
     */
    public int getLongueur() {
        return this.longueur;
    }

    /**
     * teste si la sequence est vide
     * 
     * @return true si la séquence est vide, false sinon
     */
    public boolean estVide() {
        return tête == null;
    }

    /**
     * Récupérer la valeur (entier) d'un élément de la séquence désigné par son rang
     * 
     * @param rang entier compris entre 1 et la longueur de la séquence
     * @return la valeur de l'élément de rang rang
     * @throws IllegalArgumentException si rang n'est pas une valeur valide (valeur dans l'intervale
     *         [1 .. longueur de la séquence])
     */
    public Rational getValeurDeRang(int rang) {
        if (rang < 1 || rang > longueur) {
            throw new IllegalArgumentException("Rang invalide");
        }
        ElementSeqRationals eltCour = tête;
        int i = 1;
        while (i < rang) {
            eltCour = eltCour.getSuivant();
            i++;
        }
        return eltCour.getValeur();
    }

    /**
     * Retire le premier élément de la séquence
     * 
     * @return la valeur de l'élément retiré
     * @throws UnsupportedOperationException si la séquence est vide
     */
    public Rational retirerPremierElt() {
        if (this.estVide()) {
            throw new UnsupportedOperationException("opération impossible sur une séquence vide");
        }
        Rational valeur = tête.getValeur();
        tête = tête.getSuivant();
        if (tête != null) {
            tête.setPrécédent(null);
        } else {
            queue = null;
        }
        longueur--;
        return valeur;
    }

    /**
     * Retire le dernier élément de la séquence
     * 
     * @return la valeur de l'élément retiré
     * @throws UnsupportedOperationException si la séquence est vide
     */
    public Rational retirerDernierElt() {
        if (this.estVide()) {
            throw new UnsupportedOperationException("opération impossible sur une séquence vide");
        }
        Rational valeur = queue.getValeur();
        queue = queue.getPrécédent();
        if (queue != null) {
            queue.setSuivant(null);
        } else {
            tête = null;
        }
        longueur--;
        return valeur;
    }

    /**
     * Rechercher le rang d'une valeur donnée dans la séquence.
     * 
     * @param valeur la valeur à rechercher
     * @return un entier qui correspond au rang de la première occurence de la valeur dans la
     *         séquence en partant du début de celle-ci (le rang est un entier compris entre 1 et la
     *         longueur de la séquence inclus) Si la valeur recherchée n'est pas présente la méthode
     *         retourne -1.
     */
    public int rangDe(Rational valeur) {
        return rangDe(valeur, 1);
    }

    /**
     * Rechercher le rang d'une valeur donnée dans la séquence à partir d'un rang donné.
     * 
     * @param valeur la valeur à rechercher
     * @param rangDepart rang à partir duquel la recherche est effectuée
     * @return retourne un entier qui correspond au rang de la première occurence de la valeur dans
     *         la séquence en partant du rang donné. Si la valeur recherchée n'est pas présente la
     *         méthode retourne -1.
     */
    public int rangDe(Rational valeur, int rangDepart) {
        if (rangDepart < 1 || rangDepart > longueur) {
            throw new IllegalArgumentException("Rang invalide");
        }
        ElementSeqRationals eltCour = tête;
        int i;
        // on saute les éléments avant rangDepart
        for (i = 1; i < rangDepart; i++, eltCour = eltCour.getSuivant());
        // on effectue la recherche à partir de rangDepart
        for (; i <= this.longueur; i++, eltCour = eltCour.getSuivant()) {
            if (eltCour.getValeur().egale(valeur)) {
                return i;
            }
        }
        // on a parcouru toute la séquence, la valeur n'a pas été trouvée
        return -1;
    }

    /**
     * Permet d'obtenir une nouvelle séquence ne contenant que les valeurs de rang impair de la
     * séquence.
     * 
     * @return la sequence ne contenant que les valeurs de rang impair
     */
    public SequenceRationals sequenceRangsImpair() {
        SequenceRationals res = new SequenceRationals();
        ElementSeqRationals eltCourant = this.tête;
        for (int i = 1; i <= this.longueur; i += 2) {
            res.ajouterEnQueue(eltCourant.getValeur());
            eltCourant =
                    (eltCourant.getSuivant() != null) ? eltCourant.getSuivant().getSuivant() : null;
        }
        return res;
    }

    /**
     * Une autre version (naive) de la méthode précédente. Si l'alogrithme de celle-ci est beaucoup
     * plus simple et probablement, le premier qui vient à l'esprit, il est moins efficace car à
     * chaque itération on refait un parcours de séquence pour trouver l'élément de rang i alors que
     * dans la solution précédente on ne fait qu'un seul parcours.
     * 
     * @return la sequence ne contenant que les valeurs de rang impair
     */
    public SequenceRationals sequenceRangsImpair2() {
        SequenceRationals res = new SequenceRationals();
        for (int i = 1; i <= this.longueur; i += 2) {
            res.ajouterEnQueue(this.getValeurDeRang(i));
        }
        return res;
    }

    /**
     * Retourne une chaîne de caractères représentant la séquence.
     * 
     * @return "(1/2, 4/5, 6/7, 7/3)"" si la séquence contient les valeurs 1/2, 4/5, 6/7 et 7/3;
     *         "()" si la séquence est vide.
     */
    public String toString() {
        StringBuilder resultat = new StringBuilder("("); // pour faire des concaténations de String
                                                         // c'est plus efficace
        boolean premier = true;
        for (ElementSeqRationals eltCourant = tête; eltCourant != null; eltCourant =
                eltCourant.getSuivant()) {
            if (premier) {
                resultat.append(eltCourant.getValeur());
                premier = false;
            } else {
                resultat.append(", ").append(eltCourant.getValeur());
            }
        }
        resultat.append(")");
        return resultat.toString();
    }

}
